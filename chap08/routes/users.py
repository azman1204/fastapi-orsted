from fastapi import APIRouter, Body, HTTPException, status, Depends
from models.event import Event
from typing import List
from models.user import User, UserSignIn, UserBase
from database.connection import get_session
from auth.hash_password import HashPassword
from fastapi.security import OAuth2PasswordRequestForm # html form to login
from auth.jwt_handler import create_access_token

user_router = APIRouter()

# create a new user
@user_router.post('/signup')
async def sign_new_user(data: User, session=Depends(get_session)) -> dict:
    # check if email already exist
    user = session.get(User, data.email) # get() - find by primary key and return an obj
    if not user:
        hp = HashPassword()
        hashed_password = hp.create_hash(data.password)
        data.password = hashed_password
        session.add(data)
        session.commit()
        return {"message": "user registered successfully"}
    
    raise HTTPException(
        status_code=status.HTTP_404_NOT_FOUND,
        detail="User already exist"
    )


# see page 152
@user_router.post("/signin")
async def sign_user_in(user: OAuth2PasswordRequestForm = Depends(), session=Depends(get_session)) -> dict :
    user_exist = session.get(User, user.username) # select * from user where email = ?

    if user_exist:
        hash_password = HashPassword()
        if hash_password.verify_hash(user.password, user_exist.password):
            # password matched
            access_token = create_access_token(user_exist.email)
            return {
                "access_token": access_token,
                "token_type": "Bearer"
            }
    
    raise HTTPException(
        status_code=status.HTTP_403_FORBIDDEN,
        detail="Wrong credentials"
    )
