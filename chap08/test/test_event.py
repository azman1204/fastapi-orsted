from fastapi.testclient import TestClient
from main import app

client = TestClient(app)

def test_list_event():
    response = client.get("/event")
    print(response.json())
    assert response.status_code == 200


def test_about():
    response = client.get("/about")
    assert response.json() == {"message": "about page"}


def test_get_one_event():
    # get the token - but post as form data / http-url-encoded
    body = {"username": "abu@gmail.com", "password": "1234"}
    response2 = client.post("/user/signin", data=body)
    data2 = response2.json()
    access_token = data2.get("access_token")

    # access_token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiYWJ1QGdtYWlsLmNvbSIsImV4cGlyZSI6MTcxMjE5OTYyOC4yMzI1MzV9.WAQMu72u6DqfxY0WsmJYOeaGm3-8L12w8WdWA0udZyI'
    headers = {"Authorization": f"Bearer {access_token}", "Content-Type":"application/json"}
    response = client.get("/event/1", headers=headers)
    assert response.status_code == 200
    data = response.json() # convert the returned JSON into dict / list

    # need to prepare data before running this test
    assert data.get("title") == "test"