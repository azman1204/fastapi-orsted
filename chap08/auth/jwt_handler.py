import time
from datetime import datetime
from fastapi import HTTPException, status
from jose import jwt, JWTError
import os
from dotenv import load_dotenv

load_dotenv() # load content from .env into memory

# see page 148
def create_access_token(user: str) -> str :
    payload = {
        "user": user,
        "expire": time.time() + 3600 # 60m x 60s = 1 hour
    }
    token = jwt.encode(payload, os.getenv("SECRET_KEY"), algorithm="HS256")
    return token


def verify_access_token(token: str) -> dict :
    try:
        # if successfully decode, return dict of ori data
        data = jwt.decode(token, os.getenv("SECRET_KEY"), algorithms=["HS256"])
        expire = data.get("expire")

        if expire is None:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="no access token supplied"
            )
        
        if datetime.utcnow() > datetime.utcfromtimestamp(expire):
            # expired already
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="invalid token"
            )

        # everything ok
        return data
    except JWTError:
        raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="invalid token"
            )