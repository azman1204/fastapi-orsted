from fastapi import APIRouter, Body, HTTPException, status
from models.event import Event
from typing import List
from models.user import User, UserSignIn

user_router = APIRouter()
users = {}

@user_router.post('/signup')
async def sign_new_user(data: User) -> dict:
    if data.email in users:
        # this email already exist
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail="User with the supplied email already exist"
        )
    
    users[data.email] = data
    return {"message": "successfully sign up a new user"}

@user_router.post("/signin")
async def sign_user_in(user: UserSignIn) -> dict :
    if user.email not in users:
        # this user does not exist
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="wrong credentials passed"
        )
    
    # check password
    if user.password != users[user.email].password:
        # wrong password
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="wrong credentials passed"
        )
    
    return {"message": "you signed in successfully"}

