from typing import List
from pydantic import BaseModel

class Event(BaseModel):
    id: int
    title: str
    image: str
    description:str
    tags: List[str] # ["abc", "dec"]
    location: str
