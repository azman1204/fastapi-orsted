import mysql.connector
# this will handle connection to database

def getConnection():
    conn = mysql.connector.connect(
        user="root",
        password="",
        host="127.0.0.1",
        database="fastapi_demo",
        port=3307
    )
    return conn