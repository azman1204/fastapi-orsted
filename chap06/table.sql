CREATE TABLE event (
 id int NOT NULL auto_increment PRIMARY KEY,
 title varchar(150),
 image varchar(50),
 description varchar(255),
 tags json,
 location varchar(100)
)

CREATE TABLE USER (
  email varchar(50) NOT NULL PRIMARY KEY,
  password varchar(100)
)

alter table user add column title varchar(50);

CREATE TABLE user_event (
  id int NOT NULL auto_increment PRIMARY KEY,
  email varchar(50) NOT null,
  event_id int NOT null
)