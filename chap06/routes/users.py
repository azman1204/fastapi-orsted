from fastapi import APIRouter, Body, HTTPException, status
from models.event import Event
from typing import List
from models.user import User, UserSignIn
from database.connection import getConnection

user_router = APIRouter()
users = {}

@user_router.post('/signup')
async def sign_new_user(data: User) -> dict:
    conn = getConnection()
    cursor = conn.cursor()
    sql = f"SELECT * FROM user WHERE email = '{data.email}'"
    cursor.execute(sql)
    row = cursor.fetchone() # fetchall() [{}, {}, {}] fetchone() {}

    if row:
        # this email already exist
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail="User with the supplied email already exist"
        )
    
    sql = f"INSERT INTO user (email, title, password) VALUES('{data.email}', '{data.title}', '{data.password}')"
    cursor.execute(sql)
    conn.commit()
    cursor.close()
    conn.close()
    return {"message": "successfully sign up a new user"}

@user_router.post("/signin")
async def sign_user_in(user: UserSignIn) -> dict :
    conn = getConnection()
    cursor = conn.cursor()
    sql = f"SELECT * FROM user WHERE email = '{user.email}' AND password = '{user.password}'"
    cursor.execute(sql)
    row = cursor.fetchone()

    if row:
        # this user exist
        return {"message": "you signed in successfully"}
    else:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="wrong credentials passed"
        )
    

