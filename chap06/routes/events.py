import json
from fastapi import APIRouter, Body, HTTPException, status
from models.event import Event
from typing import List
from database.connection import getConnection

event_router = APIRouter()
# https://gitlab.com/azman1204/fastapi-training

# update an aevent
@event_router.put('/event-update/{id}')
async def update_event(id: int, event: Event = Body()) -> dict :
    conn = getConnection()
    cursor = conn.cursor()
    sql = f"SELECT * FROM event WHERE id = {id}"
    cursor.execute(sql)
    row = cursor.fetchone()
    if row:
        # this id / event exist
        sql = f"""
                UPDATE event SET 
                    title = '{event.title}',
                    image = '{event.image}',
                    description = '{event.description}',
                    tags = '{json.dumps(event.tags)}',
                    location = '{event.location}'
                WHERE id = {id}
              """
        cursor.execute(sql)
        conn.commit()
        cursor.close()
        conn.close()
        return {"message": "Event successfully updated"}
    else:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Suplied id does not exist"
        )

# fetch all events
@event_router.get('/', response_model=List[Event])
async def retrieve_all_events() -> List[Event]:
    conn = getConnection()
    sql = "SELECT * FROM event"
    cursor = conn.cursor()
    cursor.execute(sql)
    rows = cursor.fetchall()
    event_list = []
    for row in rows:
        params = {"id":row[0], "title":row[1], "image":row[2], 
                  "description":row[3], "tags":json.loads(row[4]), "location":row[5]}
        event = Event(**params)
        event_list.append(event)

    cursor.close()
    conn.close()
    return event_list

# fetch one event
@event_router.get('/{id}', response_model=Event)
async def retrieve_event(id: int) -> Event:
    conn = getConnection()
    sql = f"SELECT * FROM event WHERE id = {id}"
    cursor = conn.cursor()
    cursor.execute(sql)
    row = cursor.fetchone()
    print(row)
    if row:
        params = {"id":row[0], "title":row[1], "image":row[2], "description":row[3], "tags":json.loads(row[4]), "location":row[5]}
        # json.loads() - deserialize a json string into List / dict
        return Event(**params)
    else:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Supplied ID does not exist"
        )

@event_router.post("/new")
async def create_event(body: Event = Body(...)) -> dict :
    conn = getConnection()
    sql = """
            INSERT INTO event (title, image, description, tags, location)
            VALUES('{}', '{}', '{}', '{}', '{}')
            """
    sql = sql.format(body.title, body.image, body.description, json.dumps(body.tags), body.location)
    # json.dumps() serialize a string into a JSON
    print(sql)
    cursor = conn.cursor()
    cursor.execute(sql)
    conn.commit()
    cursor.close()
    conn.close()
    return {
        "message": "Event created successfully"
    }

# delete an event
@event_router.delete("/{id}")
async def delete_event(id: int) -> dict :
    conn = getConnection()
    sql = f"DELETE FROM event WHERE id = {id}"
    cursor = conn.cursor()
    cursor.execute(sql)
    conn.commit()
    count = cursor.rowcount
    cursor.close()
    conn.close()

    if count > 0:
        return {"message": "Event successfully deleted"}
    else:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Event with supplied ID does not exist"
        )


@event_router.delete("/")
async def delete_all_events() -> dict :
    conn = getConnection()
    sql = f"DELETE FROM event WHERE id > 0"
    cursor = conn.cursor()
    cursor.execute(sql)
    conn.commit()
    count = cursor.rowcount
    cursor.close()
    conn.close()

    if count > 0:
        return {"message": f"{count} Event successfully deleted"}
    else:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="No event deleted"
        )