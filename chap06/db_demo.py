import mysql.connector

conn = mysql.connector.connect(
    user="root",
    password="",
    host="127.0.0.1",
    database="fastapi_demo",
    port=3307
)

# 2 techniques to db: 1. Query builder, 2. ORM - Object Relational Mapping
sql = "SELECT * FROM employee"
cursor = conn.cursor()
cursor.execute(sql)
rows = cursor.fetchall()
for row in rows:
    print(row[1])

cursor.close()
conn.close()