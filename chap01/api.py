from fastapi import FastAPI

app = FastAPI() # instantiate a FastAPI program

# API node
# get, post, put, patch, delete
# http://127.0.0.1:8000/
@app.get('/')
async def welcome() -> dict:
    return {"message": "Hello World"}


# http://127.0.0.1:8000/home
@app.get('/home')
async def home() -> dict:
    return {"message": "welcome home"}