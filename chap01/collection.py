# list
employees = ['Dhivya', 'Lo Zhi', 'Saifudin']
print(employees[1]) # Lo Zhi

# dictionary
person = {'name': 'john Doe', 'age': 45}
print(f"{person['name']} {person['age']}")

# tuple - inmutable
# order
staff = ("Abu", "Ravi", "Ah Chong")
print(staff[0])

# set - can't have same value (unique)
# no order
fruits = {'apple', 'grapes', 'Kiwi', 'apple'}
print(fruits)


# type hinting
name: str = "John Doe"
