from fastapi import APIRouter, Path, HTTPException, status
from model import TodoItems, TodoItem

router = APIRouter()
todo_list = []

@router.delete("/todo/{id}")
async def delete_single_todo(id: int) -> dict:
    for index in range(len(todo_list)):
        todo = todo_list[index]
        if todo.id == id:
            todo_list.pop(index)
            return {"message": "todo deleted successfully"}

    # id does not exist
    raise HTTPException(
        status_code=status.HTTP_404_NOT_FOUND,
        detail="ID does not exist"
    )

# http://localhost:8000/todo
@router.get("/todo", response_model=TodoItems)
async def todo_home() -> dict:
    return {"todos": todo_list}

@router.post("/todo", status_code=201)
async def add_todo(todo: TodoItem) -> dict:
    todo_list.append(todo)
    return {"todo": todo_list}

@router.get("/todo/{id}")
async def get_single_todo(id: int = 
    Path(..., title="the id of the todo to retrieve")) ->dict :
    for todo in todo_list:
        if todo.id == id:
            return {"todo": todo}
    
    # id not found
    raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                        detail="Todo with supplied ID doesn't exist")

@router.put("/todo/{id}")
async def update_todo(id: int, todo_item: TodoItem) -> dict:
    for todo in todo_list:
        if todo.id == id:
            todo.item = todo_item.item
            return {"todo": todo}
    
    # id does not exist
    raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="ID not found")