import asyncio

async def fetch_data():
    print("Fetching data...")
    # Simulate asynchronous operation (e.g., querying an API)
    await asyncio.sleep(2)
    print("fetch data...")
    return {"data": "some data"}

async def process_data():
    print("Processing data...")
    # Asynchronously fetch data
    fetch_data()
    print("Processing data 2...")
    # data = await fetch_data()
    # print("Data received:", data)
    # Simulate further processing
    await asyncio.sleep(1)
    print("Data processed.")

async def main():
    print("Start")
    # Asynchronously process data
    await process_data()
    print("End")

# Run the event loop
asyncio.run(main())