# httpx - library which act like browser pr postman

# get a list of event
import httpx
import asyncio

async def main():
    async with httpx.AsyncClient() as client:
        token = 'xxxxxxz'
        response = await client.get(f"http://localhost:8000/event/all/{token}")
        print("status code", response.status_code)
        print("Response body", response.text)

asyncio.run(main())
