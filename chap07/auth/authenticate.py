# this function will protect all nodes / routes
# see page 150

from fastapi.security import OAuth2PasswordBearer
from fastapi import Depends, HTTPException, status
from auth.jwt_handler import verify_access_token

# login page is "/user/signin"
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/user/signin")

async def authenticate(token: str = Depends(oauth2_scheme)) -> str :
    if not token:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="sign in for access"
        )
    
    # everythin ok
    decoded_token = verify_access_token(token)
    return decoded_token["user"]