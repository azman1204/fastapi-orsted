from fastapi import APIRouter, Body, HTTPException, status, Depends
from models.event import Event
from typing import List
from models.user import User, UserSignIn, UserBase
from database.connection import get_session

user_router = APIRouter()

# create a new user
@user_router.post('/signup')
async def sign_new_user(data: UserBase, session=Depends(get_session)) -> dict:
    # check if email already exist
    user = session.get(User, data.email) # get() - find by primary key and return an obj
    data2 = User(email=data.email, title=data.title, password=data.password)
    if not user:
        session.add(data2)
        session.commit()
        return {"message": "user registered successfully"}
    
    raise HTTPException(
        status_code=status.HTTP_404_NOT_FOUND,
        detail="User already exist"
    )


@user_router.post("/signin")
async def sign_user_in(user: UserSignIn) -> dict :
    pass
    

