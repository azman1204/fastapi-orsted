from fastapi import APIRouter, Body, Depends, HTTPException, status
from models.event import Event, EventUpdate
from typing import List
from database.connection import get_session
from sqlmodel import select

event_router = APIRouter()
# https://gitlab.com/azman1204/fastapi-training

# update an event - see pg 112
@event_router.put('/event-update/{id}')
async def update_event(id: int, new_data: EventUpdate, session=Depends(get_session)) -> dict :
    event = session.get(Event, id)
    if event:
        # dict() - convert obj to dict
        event_data = new_data.dict(exclude_unset=True) # unset data to remove
        
        for key, value in event_data.items(): # item() return list of key in dict
            setattr(event, key, value)
        
        session.add(event) # add() - for insert and update
        session.commit()
        session.refresh(event)
        return {"message": "Event updated successfully"}
    
    raise HTTPException(
        status_code=status.HTTP_404_NOT_FOUND,
        detail="Supplied id does not exist"
    )


# fetch all events - see page 109
@event_router.get('/', response_model=List[Event])
async def retrieve_all_events(session=Depends(get_session)) -> List[Event]:
    statement = select(Event)
    events = session.exec(statement).all()
    return events


# fetch one event
@event_router.get('/{id}', response_model=Event)
async def retrieve_event(id: int, session=Depends(get_session)) -> Event:
    event = session.get(Event, id)
    if event:
        return event
    
    raise HTTPException(
        status_code=status.HTTP_404_NOT_FOUND,
        detail="Supplied id does not exist"
    )


# Depends() - will run first before anything else
# do not send "id", as this is auto increment
@event_router.post("/new")
async def create_event(new_event: Event, session=Depends(get_session)) -> dict :
    session.add(new_event) # insert into ...
    session.commit()
    session.refresh(new_event) # refresh() - re-populate the obj.
    return {"message": "Event created successfully"}


# delete an event
@event_router.delete("/{id}")
async def delete_event(id: int, session=Depends(get_session)) -> dict :
    event = session.get(Event, id)
    if event:
        session.delete(event)
        session.commit()
        return {"message": "Event deleted successfully"}

    raise HTTPException(
        status_code=status.HTTP_404_NOT_FOUND,
        detail="Supplied id does not exist"
    )


# delete all events
@event_router.delete("/")
async def delete_all_events(session=Depends(get_session)) -> dict :
    session.query(Event).delete()
    session.commit()
    return {"message" : "All Event deleted successfully"}