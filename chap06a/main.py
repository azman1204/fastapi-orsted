from fastapi import FastAPI
from database.connection import conn
from routes.events import event_router
from routes.users import user_router
from fastapi.responses import RedirectResponse
import uvicorn

app = FastAPI()
# /user/signup, /user/create, ...
app.include_router(event_router, prefix="/event")
app.include_router(user_router, prefix="/user")

# run this automatically when startup the server
@app.on_event("startup")
def on_startup():
    conn()

# landing node
@app.get("/")
async def home():
    return RedirectResponse(url="/event/")

# if we run this python script direcly. >python main.py
if __name__ == '__main__':
    uvicorn.run("main:app", host="0.0.0.0", port=8000, reload=True)