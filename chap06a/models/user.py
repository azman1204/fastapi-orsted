from typing import List, Optional
from pydantic import BaseModel, EmailStr
from models.event import Event
from sqlmodel import SQLModel, Column, String, Field, AutoString

class User(SQLModel, table=True):
    # email: EmailStr = Field(unique=True, index=True, sa_type=AutoString, primary_key=True)
    email: str = Field(primary_key=True)
    title: str
    password: str


class UserSignIn(BaseModel):
    email: str
    password: str


class UserBase(BaseModel):
    # email: EmailStr = Field(unique=True, index=True, sa_type=AutoString, primary_key=True)
    email: EmailStr
    title: str
    password: str