from fastapi import APIRouter
from model import TodoItems

router = APIRouter()

todo_list = ["work", "more work"]

# http://localhost:8000/todo
@router.get("/todo", response_model=TodoItems)
def todo_home() -> dict:
    # FastAPI auto convert dict to JSON
    return {"message": "todo home", }


@router.post("/todo")
def todo_home() -> dict:
    return {"todo": todo_list}