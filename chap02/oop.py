class House:
    def printMe(self):
        print("this is a house")


# this is inheritance 
class Room(House):
    def printMe2(self):
        print('this is a room')


house = House()
house.printMe() # this is a house

room = Room()
room.printMe2() # this is a room
room.printMe() # this is a house