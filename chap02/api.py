from fastapi import FastAPI
from todo import router
from book import book_router

app = FastAPI()
app.include_router(router)
app.include_router(book_router)
